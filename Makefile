SRC_FILES = $(shell find src/ -type f -name '*')
INCLUDE_FILES = $(shell find include/ -type f -name '*')
BUILD_FILES = $(shell find build/  -type f -name '*.ninja')

config: CMakeLists.txt $(BUILD_FILES) vcpkg.json
	@mkdir -p build
	@cmake -S . -B build \
		-G "Ninja" \
		-DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
		-DCMAKE_TOOLCHAIN_FILE="~/vcpkg/scripts/buildsystems/vcpkg.cmake"

build: $(SRC_FILES) $(INCLUDE_FILES)
	@cmake --build build

format:
	@clang-format -i src/**.cpp include/**.h --sort-includes

lint:
	@echo "Linting with cppcheck"
	@cppcheck --enable=all --project=build/compile_commands.json --cppcheck-build-dir=build/cppcheck

clean:
	@rm -rf build
