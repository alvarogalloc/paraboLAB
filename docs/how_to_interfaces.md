# How interfaces work in this project?

> interfaces are just objects that are passed around objects in all the app for easy access to core APIs. e.g. Input api, Graphics Api, System Api, ECS api. 

## Some things that are implemented in all interfaces in this project:
* A factory function for convenience and sake of no code repetition:
    * Example: 
    ```
    static auto graphics_api::make_api(cen::window_handle w, cen::renderer_handle r) 
    {
      struct api:graphics_api{ ...}api;
      return api;
    }
    ```
