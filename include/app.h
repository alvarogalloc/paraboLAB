#pragma once
// #include "centurion/common/math.hpp"
// #include "projectile.h"
// #include "target.h"
// #include "utils.h"
// #include <centurion.hpp>
// #include <imgui.h>
// #include <imgui_impl_sdl2.h>
// #include <imgui_impl_sdlrenderer2.h>


#include "centurion/video/renderer.hpp"
#include "centurion/video/window.hpp"
#include "core/interfaces/graphics_api.h"
#include "core/interfaces/input_api.h"
#include "projectile.h"
#include "core/interfaces/simulation.h"
#include "target.h"
#include <map>
#include <string_view>


void render_gui(float *t, float *angle, float *v0);

class App : public I_app_api
{
public:
  using simulation_ptr = std::shared_ptr<I_simulation>;

private:
  cen::window m_window;
  cen::renderer m_renderer;

  std::vector<cen::key_code> m_pressedKeys;
  cen::fpoint m_mouse_position{ 0.f, 0.f };
  std::vector<cen::mouse_button> m_pressedMouseButtons;
  std::map<std::string_view, simulation_ptr> m_simulations;
  std::pair<std::string_view, simulation_ptr> m_current_simulation;
    
  std::unique_ptr<I_graphics_api> m_graphics_api;
  std::unique_ptr<I_input_api> m_input_api;

public:
  [[nodiscard]] I_graphics_api* get_graphics_api() const override { return m_graphics_api.get(); }
  [[nodiscard]] I_input_api* get_input_api() const override { return m_input_api.get(); }
    

  void add_simulation(std::string_view name,
    const std::shared_ptr<I_simulation> &simulation)
  {
    simulation->m_app_api = this;
    m_simulations.insert({ name, simulation });
  }


  App();
  App(const App &) = delete;
  App(App &&) = delete;
  App &operator=(const App &) = delete;
  App &operator=(App &&) = delete;
  ~App() override;
  void run();

private:
  void init_imgui(SDL_Window *window, SDL_Renderer *renderer);
  void show_change_scene_window();
  void change_scene(std::string_view name);
};
