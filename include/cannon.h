/*
 * things this needs
 * draw a "cannon" that is really
 * a spring of 1m.
 * Things this struct should hold:
 * - Stiffness of spring
 * - compression of the spring
 * - position
 * functionality:
 * - given the velocity, calculate te compression needed
 * - draw function that draws a spring with the current compression
 */

#pragma once
#include "centurion/common/math.hpp"
#include "centurion/video/renderer.hpp"
#include "projectile.h"

struct cannon
{
  // this is in the thousands+ quantities so a uint_32 will do
  std::uint32_t stiffness;
  float compression;
  cen::fpoint position;
  cen::frect rect{ position, cen::farea{ 100, 20 } };
  projectile proj{ cen::fpoint{ position.x() + rect.width(), position.y() },
    10.f,
    0 };

  // functions
  float compression_needed_to_reach(const float velocity);
  void launch_projectile();
  void draw(cen::renderer_handle ren, const float angle, const float time_passed);
};
