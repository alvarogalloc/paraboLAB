
/*
 * camera that is able to pan and zoom
 */
#include "centurion/common/math.hpp"
#include <spdlog/spdlog.h>
#include <type_traits>
template<typename T> class basic_camera final
{
public:
  basic_camera() = default;

  // setters
  basic_camera &setZoom(const T &other)
  {
    if (other <= 0) return *this;
    m_zoom = other;
    return *this;
  }
  basic_camera &setPosition(const cen::basic_point<T> &other)
  {
    m_camera_rect.set_position(other);
    return *this;
  }
  basic_camera &setSize(const cen::basic_area<T> &other)
  {
    m_camera_rect.set_size(other);
    return *this;
  }
  // is the upper two functions combined
  basic_camera &setRect(const cen::basic_rect<T> &other)
  {
    m_camera_rect = other;
    return *this;
  }

  // actions
  basic_camera &zoom(const T zoom)
  {
    setZoom(m_zoom + zoom);
    return *this;
  }

  basic_camera &move(const cen::basic_point<T> &offset)
  {
    setPosition(m_camera_rect.position() + offset);
    return *this;
  }

  // use this every time you want to use this camera
  void useCamera(cen::renderer_handle ren)
  {
    // m_camera_rect.set_size({ m_camera_rect.width() * getZoom().x(),
    //   m_camera_rect.height() * getZoom().y() });
    // auto new_rect = ren.viewport();
    // new_rect.set_position(getPosition().x(), getPosition().y());
    // if constexpr (std::is_floating_point_v<T>)
    //   ren.set_viewport(new_rect.as_i());
    // else
    ren.set_scale({m_zoom, m_zoom});
    ren.set_viewport({getPosition().as_i(), getSize().as_i()});
    // ren.set_clip({ 0, 0, getSize().as_i().width, getSize().as_i().height });
    // ren.set_scale({ m_zoom, m_zoom });
  }

  // accessors
  T getZoom() const { return m_zoom; }
  cen::basic_point<T> getPosition() const { return m_camera_rect.position(); }
  cen::basic_area<T> getSize() const { return m_camera_rect.size(); }
  cen::basic_rect<T> getRect() const { return m_camera_rect; }

  ~basic_camera() = default;

  cen::basic_rect<T> m_camera_rect;
  T m_zoom;
};

using Camera = basic_camera<float>;
