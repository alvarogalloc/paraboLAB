#pragma once

#include "core/interfaces/graphics_api.h"
#include "core/interfaces/input_api.h"

struct I_app_api// NOLINT
{
  virtual ~I_app_api() = default;
  [[nodiscard]] virtual I_graphics_api* get_graphics_api() const = 0; 
  [[nodiscard]] virtual I_input_api* get_input_api() const = 0;
};
