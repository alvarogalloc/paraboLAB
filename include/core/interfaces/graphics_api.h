#pragma once
#include "centurion/fonts/font.hpp"
#include "centurion/io/paths.hpp"
#include "centurion/video/renderer.hpp"
#include "centurion/video/surface.hpp"
#include "centurion/video/window.hpp"
#include <imgui.h>

struct I_graphics_api// NOLINT
{
  using Fn = std::function<void()>;

  virtual cen::window_handle get_window() = 0;
  virtual cen::renderer_handle get_renderer() = 0;
  virtual void add_gui_function(Fn fn) = 0;
  virtual void draw_gui() = 0;
  virtual cen::surface make_text(const std::string_view) = 0;
  virtual ~I_graphics_api() = default;

  static std::unique_ptr<I_graphics_api> make_api(cen::window_handle w,
    cen::renderer_handle r,
    const std::string &font_path)
  {
    struct api : I_graphics_api
    {
      api(cen::window_handle w,
        cen::renderer_handle r,
        const std::string &font_name)
        : w(w), r(r), m_font{ cen::base_path().copy() + font_name, 48 }
      {}
      cen::window_handle get_window() override { return w; }
      cen::renderer_handle get_renderer() override { return r; }
      void add_gui_function(I_graphics_api::Fn fn) override
      {
        gui_functions.emplace_back(fn);
      }
      void draw_gui() override
      {
        for (const auto &fn : gui_functions) { fn(); }
      }
      [[nodiscard]]cen::surface make_text(const std::string_view str) override
      {
        return m_font.render_blended(str.data(), r.get_color());
      }
      cen::window_handle w;
      cen::renderer_handle r;
      std::vector<Fn> gui_functions;
      cen::font m_font;
    };
    return std::make_unique<api>(w, r, font_path);
  }
};
