#pragma once


#include "centurion/common/math.hpp"
#include "centurion/events/event_handler.hpp"
#include "centurion/events/event_type.hpp"
#include "centurion/input/mouse.hpp"
#include <centurion/input.hpp>
#include <fmt/core.h>
#include <imgui_impl_sdl2.h>
#include <span>
#include <spdlog/spdlog.h>

struct I_input_api// NOLINT
{
  using event_hook = std::function<void(cen::event_handler)>;
  virtual void update(cen::event_handler ev) = 0;
  [[nodiscard]] virtual bool is_key_pressed(cen::key_code key) const = 0;
  [[nodiscard]] virtual bool is_key_down(cen::key_code key) const = 0;
  [[nodiscard]] virtual bool is_key_released(cen::key_code key) const = 0;

  virtual void add_event_handler(cen::event_type type, event_hook fn) = 0;

  [[nodiscard]] virtual cen::fpoint get_mouse_position() const = 0;

  [[nodiscard]] virtual bool is_mouse_button_pressed(
    cen::mouse_button button) const = 0;
  [[nodiscard]] virtual bool is_mouse_button_down(
    cen::mouse_button button) const = 0;
  [[nodiscard]] virtual bool is_mouse_button_released(
    cen::mouse_button button) const = 0;
  [[nodiscard]] virtual cen::ipoint get_mouse_scroll() const = 0;


  virtual ~I_input_api() = default;
  static std::unique_ptr<I_input_api> make_api()
  {
    struct api : I_input_api
    {
      std::vector<cen::key_code> pressed_keys;
      std::vector<cen::key_code> down_keys;
      std::vector<cen::key_code> released_keys;
      cen::ipoint mouse_scroll;

      std::vector<cen::mouse_button> pressed_mouse_buttons;
      std::vector<cen::mouse_button> down_mouse_buttons;
      std::vector<cen::mouse_button> released_mouse_buttons;

      std::unordered_map<cen::event_type, event_hook> event_handlers;
      cen::fpoint mouse_position;

      void update(cen::event_handler ev) override
      {
        pressed_keys.clear();
        released_keys.clear();
        pressed_mouse_buttons.clear();
        released_mouse_buttons.clear();
        mouse_scroll = { 0, 0 };
        while (ev.poll())
        {
          if (not ev.type()) continue;
          // fmt::print("Event {}\n", cen::to_string(ev.type().value()));
          ImGui_ImplSDL2_ProcessEvent(ev.data());
          const auto type = ev.type().value();// NOLINT
          if (event_handlers.contains(type)) { event_handlers[type](ev); }
          switch (type)
          {
          case cen::event_type::key_down: {

            auto key_event = ev.get<cen::keyboard_event>();
            pressed_keys.push_back(key_event.key());
            down_keys.push_back(key_event.key());
            break;
          }
          case cen::event_type::key_up: {
            auto key_event = ev.get<cen::keyboard_event>();
            released_keys.push_back(key_event.key());
            down_keys.erase(
              std::remove(down_keys.begin(), down_keys.end(), key_event.key()),
              down_keys.end());
            break;
          }
          case cen::event_type::mouse_button_down: {
            auto mouse_event = ev.get<cen::mouse_button_event>();
            pressed_mouse_buttons.push_back(mouse_event.button());
            down_mouse_buttons.push_back(mouse_event.button());
            break;
          }
          case cen::event_type::mouse_button_up: {
            auto mouse_event = ev.get<cen::mouse_button_event>();
            released_mouse_buttons.push_back(mouse_event.button());
            down_mouse_buttons.erase(std::remove(down_mouse_buttons.begin(),
                                       down_mouse_buttons.end(),
                                       mouse_event.button()),
              down_mouse_buttons.end());
            break;
          }
          case cen::event_type::mouse_motion: {
            auto mouse_event = ev.get<cen::mouse_motion_event>();
            mouse_position = cen::fpoint{ static_cast<float>(mouse_event.x()),
              static_cast<float>(mouse_event.y()) };
            break;
          }
          case cen::event_type::mouse_wheel: {
            auto mouse_event = ev.get<cen::mouse_wheel_event>();
            mouse_scroll = { mouse_event.x(), mouse_event.y() };
            break;
          }
          default:
            break;
          }
        }
      }
      [[nodiscard]] bool is_key_pressed(cen::key_code key) const override
      {
        return std::ranges::find(pressed_keys, key) != pressed_keys.end();
      }

      [[nodiscard]] bool is_key_down(cen::key_code key) const override
      {
        return std::ranges::find(down_keys, key) != down_keys.end();
      }

      [[nodiscard]] bool is_key_released(cen::key_code key) const override
      {
        return std::ranges::find(released_keys, key) != released_keys.end();
      }

      void add_event_handler(cen::event_type type, event_hook fn) override
      {
        event_handlers[type] = std::move(fn);
      }

      [[nodiscard]] cen::fpoint get_mouse_position() const override
      {
        return mouse_position;
      }

      [[nodiscard]] bool is_mouse_button_pressed(
        cen::mouse_button button) const override
      {
        return std::ranges::find(pressed_mouse_buttons, button)
               != pressed_mouse_buttons.end();
      }


      [[nodiscard]] bool is_mouse_button_down(
        cen::mouse_button button) const override
      {
        return std::ranges::find(down_mouse_buttons, button)
               != down_mouse_buttons.end();
      }
      [[nodiscard]] bool is_mouse_button_released(
        cen::mouse_button button) const override
      {
        return std::ranges::find(released_mouse_buttons, button)
               != released_mouse_buttons.end();
      }
      [[nodiscard]] cen::ipoint get_mouse_scroll() const override
      {
        return mouse_scroll;
      }
    };

    return std::make_unique<api>();
  }
};
