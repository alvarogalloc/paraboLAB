#pragma once

#include "core/interfaces/app_api.h"
#include "target.h"


struct I_simulation// NOLINT
{
  I_app_api *m_app_api{ nullptr };
  virtual ~I_simulation() = default;
  virtual void init() = 0;
  virtual void update(float dt) = 0;
};
