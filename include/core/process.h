#include <mach/mach.h>
#include <mach/mach_host.h>
#include <mach/mach_init.h>
#include <mach/mach_types.h>
#include <mach/vm_statistics.h>
#include <stdexcept>

#ifdef __APPLE__
auto get_process_data()
{
  struct stats
  {
    std::size_t memory;
    std::size_t cpu;
  };

  mach_task_basic_info_data_t info;
  mach_msg_type_number_t infoCount = MACH_TASK_BASIC_INFO_COUNT;
  if (task_info(
        mach_task_self(), MACH_TASK_BASIC_INFO, (task_info_t)&info, &infoCount)
      != KERN_SUCCESS)
  {
    throw std::runtime_error{ "Could not get task information" };
  }

  return stats{ .memory = info.resident_size / (1024 * 1024),
    .cpu = static_cast<std::size_t>(info.user_time.seconds) };
}

#else
auto get_process_data()
{
  struct ret
  {
    std::size_t memory;
    std::size_t cpu;
  };
  return ret{ 0, 0 };
}

#endif
