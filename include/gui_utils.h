#pragma once
#include "centurion/common/math.hpp"
#include <array>
#include <imgui.h>
#include <string_view>
#include <type_traits>

template<typename T>
void gui_edit_vec2(const std::string_view name, cen::basic_point<T> *vec)
{
  std::array<T, 2> data{ vec->x(), vec->y() };
  if constexpr (std::is_floating_point_v<T>)
    ImGui::InputFloat2(name.data(), data.data());
  else
    ImGui::InputInt2(name.data(), data.data());
  vec->set_x(data[0]);
  vec->set_y(data[1]);
}
template<typename T>
void gui_edit_rect(const std::string_view name, cen::basic_rect<T> *rect)
{
  std::array<T, 4> data{ rect->x(), rect->y(), rect->width(), rect->height() };
  if constexpr (std::is_floating_point_v<T>)
    ImGui::InputFloat4(name.data(), data.data());
  else
    ImGui::InputInt4(name.data(), data.data());
  rect->set_x(data[0]);
  rect->set_y(data[1]);
  rect->set_width(data[2]);
  rect->set_height(data[3]);
}
