#pragma once
#include "core/interfaces/simulation.h"
// #include "projectile.h"
#include "target.h"
#include "cannon.h"
#include "core/camera.h"

// const projectile projectile_template{ cen::fpoint{ 30, 30 }, 10, 45 };
const target target_template{ cen::fpoint{ 400, 400 } };

struct parabola_simulation final : I_simulation// NOLINT
{
  parabola_simulation();
  void init() override;
  void gui();

  void update(float dt) override;
  void camera_system();
  
  // this zooms
  Camera m_camera;
  // this represents a normal window sized camera
  Camera m_window_camera;
  cen::music m_music;
  std::unique_ptr<cen::texture> m_text_texture;
  std::string m_text_string;
  cannon m_canon{ 40000, 0, cen::fpoint{ 100, 100 } };
  bool dragging_projectile{ false };
  bool dragging_target{ false };
  float time_passed{ 0.f };
  target target{ target_template };
  std::unique_ptr<cen::texture> m_spritesheet_texture;
  cen::fpoint text_position;
  cen::frect meter_rect;
  // projectile projectile{ projectile_template };
  bool is_simulating{ false };
  // this is for real simulation with seconds irl
  constexpr static float step = 0.1f;
  ~parabola_simulation() override { m_music.pause(); }
};
