#pragma once

#include "centurion/common/math.hpp"
#include "utils.h"
#include <cassert>
#include <cmath>
#include <fmt/core.h>
#include <imgui.h>

struct projectile
{
  constexpr projectile(const cen::fpoint initial_position,
    const float initial_velocity,
    const float angle)
  {
    assert(
      angle_min <= angle && angle <= angle_max
      && fmt::format("angle should be between {} and {}", angle_min, angle_max)
           .c_str());
    assert(initial_velocity > 0 && "cannot have negative velocity");
    this->m_initial_position = initial_position;
    this->m_initial_velocity = initial_velocity;
    this->m_angle = angle;
  }

  // members
  float m_initial_velocity;
  cen::fpoint m_initial_position;
  float m_angle{ 45.f };
  float mass{ 10.f };

  // to pixel coordinates
  constexpr cen::fpoint pos(const float time_passed)
  {
    // for future c++ modules
    using std::cos, std::sin;
    const float half = 0.5f;
    auto displacement = cen::fpoint{
      (m_initial_velocity * cos(to_radians(m_angle))) * time_passed,
      (m_initial_velocity * sin(to_radians(m_angle)) * time_passed)
        + (half * gravity * time_passed * time_passed)
    };
    // adapt to window coordinates
    displacement.set_y(-displacement.y());
    return m_initial_position + displacement;
  }

  void gui_controls(float *t, bool *simulating);

  constexpr float time_when_out_of_bounds(const cen::iarea & bounds)
  {
    // time when displacement in x or y is windowsize - initial_position
    using std::cos, std::sin, std::sqrt;
    const float angle_rad = to_radians(m_angle);
    const float v_squared = m_initial_velocity * m_initial_velocity;
    const float sin_squared = sin(angle_rad) * sin(angle_rad);
    const float term1 =
      v_squared * sin_squared
      - 2 * gravity * (bounds.height - m_initial_position.y());

    const float time_x = (bounds.width - m_initial_position.x())
                         / (m_initial_velocity * cos(angle_rad));
    const float time_y =
      (sqrt(term1) - m_initial_velocity * sin(angle_rad)) / gravity;
    const float time_y2 =
      -((sqrt(term1) + m_initial_velocity * sin(angle_rad)) / gravity);

    return std::min(time_x, std::max(time_y, time_y2));
  }
};
