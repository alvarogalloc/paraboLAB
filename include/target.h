#pragma once


#include "centurion/common/math.hpp"
#include "centurion/video/renderer.hpp"
#include <cstdlib>

struct target
{
  constexpr static float default_tolerance = 50.f;

  cen::fpoint m_position;
  float m_tolerance{ default_tolerance };
  // tolerance of m_tolerance pixels on each axis
  constexpr bool check_hit(const cen::fpoint attempt)
  {
    // check if the attempt is within the tolerance
    using std::abs;
    return (abs(attempt.x() - m_position.x()) <= m_tolerance)
           && (abs(attempt.y() - m_position.y()) <= m_tolerance);
  }
  void draw(cen::renderer_handle renderer);
};
