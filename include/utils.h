#pragma once
#include "centurion/common/math.hpp"
#include <numbers>

static constexpr float angle_min = -25.f;
static constexpr float angle_max = 89.99f;
static constexpr float gravity = -9.81f;
static constexpr float pi = std::numbers::pi_v<float>;
static constexpr float time_min = 0;
static constexpr float time_max = 20;
static constexpr float vel_min = 0;
static constexpr float vel_max = 100;

constexpr float to_radians(const float degrees)
{
  return (pi / 180.0f) * degrees;// NOLINT
}


template<typename T>
  requires std::is_arithmetic_v<T>
bool is_in(cen::basic_point<T> point,
  cen::basic_point<T> attempt,
  T tolerance = T(5))
{
  using std::abs;
  return (abs(attempt.x() - point.x()) <= tolerance)
         && (abs(attempt.y() - point.y()) <= tolerance);
}
