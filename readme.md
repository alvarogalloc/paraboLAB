**Why make this when there are simulators all over the place??** 

> I want to!! also because i have a contest where this could help as a mock simulator for that

### Features:
* Adjust these params from a gui (imgui):
    - time passed
    - play/pause simulation
    - Projectile initial position
    - Angle (degrees)
    - Initial Velocity
    - Target position
* Modular simulations, right now only the parabola implemented
* Menu to change current simulation

#### TODO
* Add more simulations
* Friendlier gui
* Animations
* Live stats with the objects
