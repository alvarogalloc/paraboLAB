#include "app.h"
#include "centurion/input/keyboard.hpp"
#include <SDL_video.h>
#include <algorithm>
#include <fmt/core.h>
#include <imgui.h>
#include <imgui_impl_sdl2.h>
#include <imgui_impl_sdlrenderer2.h>
#include <string_view>
#include <utility>

App::App()

  : m_window("SimuLab by Alvaro Gallo"), m_renderer(m_window.make_renderer())
{
  SDL_DisplayMode DM;
  SDL_GetCurrentDisplayMode(0, &DM);
  m_window.set_width(DM.w);
  m_window.set_height(DM.h);
  m_window.set_resizable(true);
  init_imgui(m_window.get(), m_renderer.get());
  m_graphics_api = I_graphics_api::make_api(cen::window_handle{ m_window },
    cen::renderer_handle{ m_renderer },
    "res/BitPotion.ttf");
  m_input_api = I_input_api::make_api();
}

void App::run()
{
  m_window.show();
  bool running = true;
  cen::event_handler event;
  auto current_simulation_it{ m_simulations.begin() };
  m_current_simulation = *current_simulation_it;
  m_current_simulation.second->init();
  auto last_frame = std::chrono::high_resolution_clock::now();
  using clock = std::chrono::high_resolution_clock;

  m_input_api->add_event_handler(
    cen::event_type::quit, [&](auto &&) { running = false; });
  while (running)
  {
    const float delta_time =
      std::chrono::duration<float>(clock::now() - last_frame).count();
    last_frame = clock::now();

    m_input_api->update(event);

    if (m_input_api->is_key_pressed(cen::keycodes::q)) { running = false; }
    // change stage
    m_renderer.clear_with(cen::colors::white);
    ImGui_ImplSDLRenderer2_NewFrame();
    ImGui_ImplSDL2_NewFrame();

    ImGui::NewFrame();
    if (current_simulation_it != m_simulations.end())
      m_current_simulation.second->update(delta_time);
    // render to gui last
    const auto last_scale = m_renderer.scale();
    // remove temporarily the scale to draw the gui
    m_renderer.set_scale({ 1, 1 });
    if (ImGui::Begin(m_current_simulation.first.data()))
    {
      show_change_scene_window();
      m_graphics_api->draw_gui();
    }
    ImGui::End();
    ImGui::Render();
    ImGui_ImplSDLRenderer2_RenderDrawData(ImGui::GetDrawData());
    m_renderer.present();
    m_renderer.set_scale(last_scale);
  }
}

void App::init_imgui(SDL_Window *window, SDL_Renderer *renderer)
{
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO &io{ ImGui::GetIO() };
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

  ImGui_ImplSDL2_InitForSDLRenderer(window, renderer);
  ImGui_ImplSDLRenderer2_Init(renderer);
  ImGui::SetNextWindowCollapsed(true);
}
void App::show_change_scene_window()
{
  // make a list of all the simulations with a button to change to them
  ImGui::Text("Current Scene: %s", m_current_simulation.first.data());// NOLINT
  ImGui::Separator();
  if (ImGui::BeginListBox("Simulations"))
  {
    std::ranges::for_each(m_simulations, [this](auto &&simulation) {
      bool is_selected = (m_current_simulation == simulation);
      if (ImGui::Selectable(simulation.first.data(), is_selected))// NOLINT
      {
        m_current_simulation = simulation;
      }
    });
    ImGui::EndListBox();
  }
}

void App::change_scene(std::string_view name)
{
  auto it = std::ranges::find_if(
    m_simulations, [name](auto &&pair) { return pair.first == name; });
  if (it == m_simulations.end()) return;
  m_current_simulation = *it;
  m_current_simulation.second->init();
}

App::~App()
{
  ImGui::DestroyContext();
  m_window.hide();
}
