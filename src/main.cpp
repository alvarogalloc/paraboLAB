#include "app.h"
#include "parabola_simulation.h"
#include <fmt/core.h>
#include <memory>


constexpr auto version = "0.1";

int main(int argc, char *argv[])
{
  if (argc > 2 and std::string{ argv[1] } != "version")// NOLINT
  {
    fmt::print("version {}", version);
    return 0;
  }
  const cen::sdl sdl;
  const cen::img img;
  const cen::mix mix;
  const cen::ttf ttf;
  std::unique_ptr<App> app = std::make_unique<App>();

  app->add_simulation(
    "Simple Parabola", std::make_shared<parabola_simulation>());

  app->run();

  return 0;
}
