#include "cannon.h"
#include "centurion/video/color.hpp"

float cannon::compression_needed_to_reach(const float velocity) { return 0.f; }

void cannon::launch_projectile() {}
void cannon::draw(cen::renderer_handle ren,
  const float angle,
  const float time_passed)
{
  // draw the cannon but also the projectile. The projectile should be
  // next to the cannon
  rect = cen::frect{ position,
    cen::farea{ 100 - (100 * compression), 20 + (20 * compression) } };
  proj.m_angle = angle;
  proj.m_initial_position =
    cen::fpoint{ position.x() + rect.width(), position.y() };
  ren.set_color(cen::colors::tomato);
  ren.fill_rect(rect);
  ren.set_color(cen::colors::black);
  auto proj_pos = proj.pos(time_passed);
  proj_pos.set_x(proj_pos.x() + 10);
  proj_pos.set_y(proj_pos.y() + rect.height() / 2);
  ren.draw_circle(proj_pos, 10);
}
