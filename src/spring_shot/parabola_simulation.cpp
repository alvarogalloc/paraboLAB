#include "parabola_simulation.h"
#include "centurion/common/math.hpp"
#include "centurion/input/keyboard.hpp"
#include "centurion/io/paths.hpp"
#include "centurion/video/renderer.hpp"
#include "centurion/video/texture.hpp"
#include "core/process.h"
#include "gui_utils.h"
#include "utils.h"
#include <imgui.h>
#include <memory>
#include <spdlog/spdlog.h>
#include <string>
parabola_simulation::parabola_simulation()
  : m_music(cen::base_path().copy() + "res/startscene.ogg")
{}


void parabola_simulation::init()
{
  auto make_measure_text = [this]() {
    m_text_string = "0.00%";
    m_text_texture = std::make_unique<cen::texture>(
      m_app_api->get_graphics_api()->get_renderer().make_texture(
        m_app_api->get_graphics_api()->make_text(m_text_string)));
  };
  auto create_sprite_sheet = [this] {
    m_spritesheet_texture = std::make_unique<cen::texture>(
      m_app_api->get_graphics_api()->get_renderer().make_texture(
        cen::base_path().copy() + "res/sprites.png"));
  };
  auto start_music = [this] {
    m_music.set_volume(50);
    m_music.play(cen::music::forever);
    m_music.pause();
  };
  auto init_cameras = [this] {
    m_camera.setPosition({ 0, 0 })
      .setSize({ 4000, 4000 })
      .setZoom(1)
      .useCamera(m_app_api->get_graphics_api()->get_renderer());
    m_window_camera = m_camera;
    m_window_camera.setSize(
      m_app_api->get_graphics_api()->get_window().size().as_f());
  };

  m_app_api->get_graphics_api()->add_gui_function([this] { gui(); });
  auto init_metrics = [this]() {
    meter_rect = { { m_window_camera.getSize().width - 70, 150 + 20 },
      { 45, -150 * m_canon.compression } };
    text_position = { meter_rect.x() + (meter_rect.width() / 2)
                        - m_text_texture->size().as_f().width / 2,
      meter_rect.y() + 20 };
  };
  // m_app_api->get_input_api()->add_event_handler(
  //   cen::event_type::window, [this](cen::event_handler ev) {
  //     if (const auto event = ev.get<cen::window_event>();
  //         event.event_id() == cen::window_event_id::resized)
  //     {
  //       m_camera.setSize(
  //         m_app_api->get_graphics_api()->get_window().size().as_f());
  //     }
  //   });
  make_measure_text();
  create_sprite_sheet();
  start_music();
  init_cameras();
  init_metrics();
}

void parabola_simulation::gui()
{
  ImGui::Separator();
  ImGui::Text("Camera");
  const auto &ren = m_app_api->get_graphics_api()->get_renderer();
  gui_edit_rect("Edit camera rect", &m_camera.m_camera_rect);
  ImGui::Text("Viewport %s", cen::to_string(ren.viewport()).c_str());
  if (ren.clip().has_value())
    ImGui::Text("clip %s", cen::to_string(ren.clip().value()).c_str());

  ImGui::SliderFloat("zoom", &m_camera.m_zoom, 1, 10);
  if (ImGui::Button("Reset camera"))
  {
    auto win = this->m_app_api->get_graphics_api()->get_window();
    m_camera.setZoom(1);
    m_camera.setRect({ 0, 0, float(win.width()), float(win.height()) });
  }
  ImGui::Separator();
  ImGui::Text("Music");
  if (m_music.is_paused())
  {
    if (ImGui::Button("Play Music")) (void)m_music.resume();
  } else if (m_music.is_playing())
  {
    if (ImGui::Button("Stop Music")) m_music.pause();
  }
  ImGui::Text("Volume");
  int volume = m_music.volume();
  ImGui::SliderInt("Volume", &volume, 0, 124);
  if (volume != m_music.volume()) { m_music.set_volume(volume); }
  ImGui::Separator();

  ImGui::Text("Simulation");
  ImGui::Text("%s",
    fmt::format(
      "Is hitting {}", target.check_hit(m_canon.proj.pos(time_passed)))
      .c_str());
  ImGui::Text("Tweak Parabola settings");

  ImGui::SliderFloat("Time passed: ", &time_passed, 0.f, 20.f);
  ImGui::Checkbox("Playing", &is_simulating);

  ImGui::Separator();
  ImGui::Text("Projectile");
  gui_edit_vec2("Projectile Position", &m_canon.proj.m_initial_position);
  ImGui::SliderFloat("Angle (degrees)", &m_canon.proj.m_angle, -89.0f, 90.0f);
  ImGui::SliderFloat(
    "Initial Velocity", &m_canon.proj.m_initial_velocity, 0.f, 200.f);
  ImGui::Separator();
  ImGui::Text("Target");
  gui_edit_vec2("Target Position", &target.m_position);
  ImGui::Separator();
  ImGui::Text("Cannon");
  gui_edit_vec2("Cannon Position", &m_canon.position);
  ImGui::SliderFloat("Compression", &m_canon.compression, .0f, 1.0f);
  if (auto str = fmt::format("{:.4}%", m_canon.compression * 100);
      str != m_text_string)
  {
    m_text_texture = std::make_unique<cen::texture>(
      m_app_api->get_graphics_api()->get_renderer().make_texture(
        m_app_api->get_graphics_api()->make_text(str)));
  }
  int stiffness = static_cast<int>(m_canon.stiffness) / 1000;

  ImGui::SliderInt("Stiffness (x1000)", &stiffness, 0, 100);
  m_canon.stiffness = static_cast<std::uint32_t>(stiffness * 1000);


  ImGui::Separator();
  ImGui::Text("Global UI");
  gui_edit_vec2("Text position", &text_position);
  gui_edit_rect("Edit meter rect", &meter_rect);
  ImGui::Separator();
  ImGui::Text("Mouse");
  ImGui::Text("position %s",
    cen::to_string(m_app_api->get_input_api()->get_mouse_position()).c_str());
  ImGui::Separator();
  ImGui::Text("Process data");
  auto [memory, cpu] = get_process_data();
  ImGui::Text("memory usage: %lu", memory);
  ImGui::Text("cpu usage: %lu", cpu);
}


void parabola_simulation::camera_system()
{
  auto &input = *m_app_api->get_input_api();
  const float pan_speed{ 10.f };
  const auto scroll = input.get_mouse_scroll().as_f();
  const cen::fpoint movement{ pan_speed * -scroll.x(), pan_speed * scroll.y() };

  if (auto [dx, dy] = movement.get(); dx != 0 or dy != 0)
    m_camera.move(movement);
  // if (m_camera.getPosition().x() > 0)
  //   m_camera.setPosition({ 0, m_camera.getPosition().y() });
  // else if (m_camera.getPosition().x()
  //          < -(m_camera.getSize().width
  //              - m_app_api->get_graphics_api()->get_window().width()))
  //   m_camera.setPosition(
  //     { -(m_camera.getSize().width
  //         - m_app_api->get_graphics_api()->get_window().width()),
  //       m_camera.getPosition().y() });
  // if (m_camera.getPosition().y() > 0)
  //   m_camera.setPosition({ m_camera.getPosition().x(), 0 });
  //
  // else if (m_camera.getPosition().y()
  //          < -(m_camera.getSize().height
  //              - m_app_api->get_graphics_api()->get_window().height()))
  //   m_camera.setPosition({ m_camera.getPosition().x(),
  //     -(m_camera.getSize().height
  //       - m_app_api->get_graphics_api()->get_window().height()) });
}

void parabola_simulation::update(float)
{
  auto &input_api = *m_app_api->get_input_api();

  // TODO: amke this to not make innecesary calls
  camera_system();


  auto ren = m_app_api->get_graphics_api()->get_renderer();
  auto &graphics_api = *m_app_api->get_graphics_api();

  m_camera.useCamera(ren);

  const auto draw_target_size = m_camera.getSize();
  if (input_api.is_mouse_button_pressed(cen::mouse_button::left)
      and input_api.is_key_down(cen::keycodes::left_shift))
  {
    m_canon.proj.m_initial_position = input_api.get_mouse_position();
    time_passed = 0;
  }
  if (is_simulating)
  {
    time_passed += step;
    if (time_passed
        > m_canon.proj.time_when_out_of_bounds(draw_target_size.as_i()))
    {
      is_simulating = false;
      time_passed = 0.f;
    }
  };
  if (input_api.is_key_pressed(cen::keycodes::enter))
  {
    is_simulating = true;
    time_passed = 0.f;
  }
  const float projectile_radius = 10;

  const auto mouse_pos{ input_api.get_mouse_position() };
  if (input_api.is_mouse_button_pressed(cen::mouse_button::left))
  {
    if (is_in(m_canon.proj.pos(time_passed), mouse_pos, projectile_radius))
      dragging_projectile = true;
    else if (is_in(target.m_position, mouse_pos, projectile_radius))
      dragging_target = true;
  }
  if (input_api.is_mouse_button_released(cen::mouse_button::left))
  {
    dragging_projectile = false;
    dragging_target = false;
  }
  if (dragging_projectile) m_canon.proj.m_initial_position = mouse_pos;
  if (dragging_target) target.m_position = mouse_pos;

  // update
  ren.clear_with(cen::colors::tan);
  ren.fill_with(cen::colors::sandy_brown);
  target.draw(ren);
  // draws also the projectile
  m_canon.draw(ren, m_canon.proj.m_angle, time_passed);


  ren.render(*m_spritesheet_texture,
    cen::irect{ 32, 32, 16, 31 },
    cen::irect{ 400, 400, 120, 240 },
    90 - m_canon.proj.m_angle,
    cen::ipoint{ 60, 208 },
    cen::renderer_flip::none);

  // this is to draw the metrics and global ui
  // m_window_camera.useCamera(ren);
  {
    const auto world_to_screen_coords = [this](
                                          const cen::fpoint p) -> cen::fpoint {
      return { p.x() - m_camera.getPosition().x(),
        p.y() - m_camera.getPosition().y() };
    };
    ren.fill_circle(world_to_screen_coords(mouse_pos), 5.f);
    // draw metrics of the how compressed the spring is

    ren.set_color(cen::colors::red);
    meter_rect.set_height(-150 * m_canon.compression);
    meter_rect.set_position(world_to_screen_coords(meter_rect.position()));
    ren.fill_rect(meter_rect);
    ren.render(*m_text_texture, text_position);
  }
}
