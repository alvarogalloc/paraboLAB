#include <fmt/core.h>
#include <imgui.h>
#include <projectile.h>


void projectile::gui_controls(float *t, bool *simulating)
{
  ImGui::Begin("Projectile Parameters");

  // FIX: this is ugly
  std::array<float, 2> position{ m_initial_position.x(),
    m_initial_position.y() };
  ImGui::InputFloat2("Projectile Position", position.data());
  m_initial_position = cen::fpoint{ position[0], position[1] };

  ImGui::SliderFloat("Time passed in seconds (t)", t, time_min, time_max);
  ImGui::SliderFloat("Angle in degrees", &m_angle, angle_min, angle_max);
  ImGui::SliderFloat(
    "Initial Velocity (V0)", &m_initial_velocity, vel_min, vel_max);
  ImGui::Text("pos %s", cen::to_string(this->pos(*t)).data());// NOLINT
  if (ImGui::Button(*simulating ? "Stop Simulating" : "Start Simulating"))
  {
    *simulating = !*simulating;
  }
  ImGui::End();
}
