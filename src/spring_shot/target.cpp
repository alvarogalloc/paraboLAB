#include "target.h"
#include "centurion/video/color.hpp"


void target::draw(cen::renderer_handle renderer)
{
  // draw three triangles
  renderer.set_color(cen::colors::black);
  renderer.fill_circle(m_position, m_tolerance + 2);
  renderer.set_color(cen::colors::red);
  renderer.fill_circle(m_position, m_tolerance);

  renderer.set_color(cen::colors::white);
  renderer.fill_circle(m_position, m_tolerance / 2);
}
